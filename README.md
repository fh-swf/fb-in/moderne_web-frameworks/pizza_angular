# Zentraler Pizza-Service der Fachhochschule Südwestfalen – Angular Version
Dies ist die Angular-Version der Beispielanwendung zum Modul [Moderne Web-Frameworks](https://elearning.fh-swf.de/course/view.php?id=6597).

## Installation und Test der Anwendung
Die folgende Beschreibung setzt eine Installation von [Node.js](https://nodejs.org) und `npm` voraus. 

Nachdem Sie das Repository geclont haben, sollten Sie zunächst mit 
```
npm install
```
die Abhängigkeiten installieren.

## Konfiguration des Backends
Das verwendete Backend können Sie in der Datei `src/app/pizza.service.ts` konfigurieren. Der voreingestellte Server `https://jupiter.fh-swf.de/pizza/api` erlaubt via *CORS-Header* einen freizügigen Zugriff.

## Start des Entwicklungsservers
Angular bringt einen integrierten Entwicklungsserver mit, der mit `ng serve` gestartet wird.
Navigieren Sie anschließend zu `http://localhost:4200/`. Bei Änderungen des Codes wird die Anwendung automatisch neu geladen.