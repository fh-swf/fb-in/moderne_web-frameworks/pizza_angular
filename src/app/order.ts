export const EMPTY_ORDER = {
    customer: {
      name: '',
      email: '',
      phone: '',
      location: null
    },
    choice: {
      choice: '',
      size: 'medium',
      extras: []
    }
  };
