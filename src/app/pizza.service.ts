import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class PizzaService {

    constructor(private http: HttpClient) {
    }

    // The code for the backend is available at https://gitlab.com/fh-swf/fb-in/inet_sose2019/tree/master/pizza/nodejs
    // Change this URL if you want to use another backend (i.e. a local one)
    baseUrl = 'https://jupiter.fh-swf.de/pizza/api';

    getOrders() {
        return this.http.get(this.baseUrl + '/order');
    }

    createOrder(order: any) {
        console.log('createOrder: %j', order);
        return this.http.post(this.baseUrl + '/order', order).pipe(
            map((result: any) => {
                console.log('POST: %o', result);
                return result.ops[0];
            }));
    }

    deleteOrder(order: any) {
        console.log('deleteOrder');
        this.http.delete(this.baseUrl + `/order/${order._id}`).subscribe(res => console.log('deleteOrder: %o', res));
    }

    saveOrder(order: any) {
        console.log('saveOrder');
        const id = order._id;
        delete order._id;
        return this.http.put(this.baseUrl + `/order/${id}`, order).pipe(
            map((result: any) => {
                console.log('PUT: %o', result);
                const ord = result.ops[0];
                ord._id = id;
                return ord;
            }));
    }
}

