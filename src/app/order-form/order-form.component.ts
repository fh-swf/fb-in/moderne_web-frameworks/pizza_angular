import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-order-form',
  templateUrl: './order-form.component.html',
  styleUrls: ['./order-form.component.css']
})
export class OrderFormComponent {

  @Input() order;
  @Output() result = new EventEmitter();

  constructor() { }

  cancelOrder(order) {
    console.log('cancelOrder: %o', order);
    this.result.emit(null);
  }

  saveOrder(order) {
    console.log('saveOrder: %o', order);
    this.result.emit(order);
  }
}
