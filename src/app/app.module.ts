import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';

import { AppComponent } from './app.component';
import { OrderFormComponent } from './order-form/order-form.component';
import { CustomerComponent } from './customer/customer.component';
import { ChoiceComponent } from './choice/choice.component';
import { LieferzeitComponent } from './lieferzeit/lieferzeit.component';

@NgModule({
  declarations: [
    AppComponent,
    OrderFormComponent,
    CustomerComponent,
    ChoiceComponent,
    LieferzeitComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

registerLocaleData(localeDe, 'de');
registerLocaleData(localeDe, 'de-DE');
