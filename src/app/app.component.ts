import { Component, OnInit } from '@angular/core';
import { PizzaService } from './pizza.service';
import { deepCopy } from './deepCopy';
import { EMPTY_ORDER } from './order';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  orders = [];
  currentOrder = null;

  constructor(private pizzaService: PizzaService) {
    this.pizzaService.getOrders().subscribe({
      next: (orders) => {
        return this.orders = orders as Array<any>;
      },
      error: (err) => console.error('getOrder: error: %o', err)
    });
  }

  edit(order) {
    console.log('edit: %o', order);
    this.currentOrder = deepCopy(order);
  }

  delete(order) {
    console.log('delete: %o', order);
    this.pizzaService.deleteOrder(order);
    this.orders = this.orders.filter(old => old._id !== order._id);
  }

  newOrder() {
    this.currentOrder = EMPTY_ORDER;
  }

  handleResult(order) {
    console.log('handleResult: %o', order);
    if (order) {
      if (order._id) {
        this.pizzaService.saveOrder(order).subscribe(res => {
          this.orders.forEach((old, index) => {
            if (old._id === res._id) {
              this.orders[index] = res;
            }
          });
        });
      } else {
        this.pizzaService.createOrder(order).subscribe(res => this.orders.push(res));
      }
    }

    // close order edit form
    this.currentOrder = null;
  }
}
