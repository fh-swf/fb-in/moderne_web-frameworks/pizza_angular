import { Component, OnInit, Input } from '@angular/core';
import { ControlContainer, NgForm } from '@angular/forms';

@Component({
  selector: 'app-choice',
  templateUrl: './choice.component.html',
  styleUrls: ['./choice.component.css'],
  viewProviders: [ { provide: ControlContainer, useExisting: NgForm } ]
})
export class ChoiceComponent implements OnInit {

  EXTRAS = [ 'Mozzarella', 'Zwiebeln', 'Champignons', 'Salami'];

  @Input() value;

  constructor() { }

  ngOnInit() {
  }

  hasExtra(extra: string) {
    return this.value.extras.indexOf(extra.toLowerCase()) >= 0;
  }

  onChange(event) {
    console.log('onChange: %o', event);
    const key = event.target.value.toLowerCase();
    if (event.target.checked) {
      if (this.value.extras.indexOf(key) < 0) {
        this.value.extras.push(key);
      }
    } else {
      this.value.extras = this.value.extras.filter(extra => extra !== key);
    }
  }
}
