import { Component, OnInit, Input, Output, EventEmitter, forwardRef } from '@angular/core';
import { ControlContainer, NgForm, ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, UntypedFormControl } from '@angular/forms';

@Component({
  selector: 'app-lieferzeit',
  templateUrl: './lieferzeit.component.html',
  styleUrls: ['./lieferzeit.component.css'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => LieferzeitComponent),
      multi: true
    }, 
    {
      provide: NG_VALIDATORS,
      useValue: (c: UntypedFormControl) => {
        console.log('validation! err: %o', c);
        const date = new Date(c.value);
        if (date < new Date()) {
          console.log('validation error');
          return {error: 'aufgrund eines Problems mit unserer Zeitmachine liefern wir aktuell nicht in die Vergangenheit!'};
        } else {
          return null;
        }
      },
      multi: true
    }
  ]
})
export class LieferzeitComponent implements ControlValueAccessor {

  @Input() value: string;
  time: string;
  date: string;
  error: string;
  propagateChange = (_: any) => { };

  constructor() { }

  writeValue(value: any): void {
    let date;
    if (!value) {
      date = new Date(Date.now() + 3600 * 1000);
      this.value = date.toISOString();
    } else {
      this.value = value;
      date = new Date(this.value);
    }
    this.date = this.getDate(date);
    this.time = this.getTime(date);
    this.propagateChange(this.value);
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    //throw new Error("Method not implemented.");
  }

  getTime(date) {
    return date.toLocaleTimeString().substring(0, 5);
  }

  getDate(date) {
    return date.toISOString().substring(0, 10);
  }

  change(event) {
    if (event.target.name === 'date') {
      this.date = event.target.value;
    } else {
      this.time = event.target.value;
    }
    console.log('change: %s %s %s', event.target.name, this.date, this.time);

    const value = Date.parse(this.date + ' ' + this.time);
    console.log('date_time: ' + value);
    this.error = null;
    if (value < Date.now()) {
      // tslint:disable-next-line:max-line-length
      this.error = 'Entschuldigen Sie bitte, aufgrund eines Problems mit unserer Zeitmachine liefern wir aktuell nicht in die Vergangenheit!';
    }
    this.value = new Date(value).toISOString();
    this.propagateChange(this.value);
  }

}
