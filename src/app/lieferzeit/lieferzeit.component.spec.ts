import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LieferzeitComponent } from './lieferzeit.component';

describe('LieferzeitComponent', () => {
  let component: LieferzeitComponent;
  let fixture: ComponentFixture<LieferzeitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LieferzeitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LieferzeitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
